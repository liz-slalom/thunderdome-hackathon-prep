#### Version control with your team  

1. One person from your team will **fork** the repo.  
  * Click the plus sign in the left sidebar  
  * Select "Fork this repository"  
  
2. Each team member will **clone** the forked repo. Navigate to the URL of the forked repo, and copy the clone command at the top of the page. It should look something like this:  
`$ git clone https://<your username>@bitbucket.org/<username of fork>/thunderdome-hackathon-prep.git`

3. Open Git Bash, navigate to your project directory of choice, and copy the clone command from step 2. Then navigate into the cloned directory:  
`$ cd thunderdome-hackathon-prep`  

4. Create a **branch** of the repository:  
`$ git checkout -b <branch-name>`  
Choose a name that is descriptive of the work that will be done in this branch. For example: `liz-python-model`  
Note all the current branches that exist on this repo: `$ git branch`  

5. Write code as you normally would 


#### Saving your changes 

Add and commit your code frequently: do these steps any time you make a change to the project. A good rule of thumb is to commit your code any time you have "completed a thought".  

Push code to your branch a bit less frequently: do this any time you have a new working version of the project.  


| | |
| --- | --- |
|`$ git status` | List the files that have been updated (note that anything listed in `.gitignore` will *not* be tracked by Git |
|`$ git add <filename> <another filename> ...` | Add ("stage") files to the current commit |
|`$ git commit -m "Commit message"` | Store all changes in the staged files with a descriptive commit message |
|`$ git push origin <branch-name>` | Push the changes to your branch on Bitbucket |


#### Pushing & Pulling

Once you have a new working version of the project that you want to share with teammates, you should [push and pull](https://www.atlassian.com/git/tutorials/making-a-pull-request).  

1. **Pushing to Branch**: `$ git push origin <branch-name>`  
You will see a message in your terminal that includes the following:  
`Create pull request for <branch-name> => master: <url>`  
Note that you do not need to create a pull request unless you want to merge your changes into the master branch (see step 2). After pushing, you (and the rest of your team) will be able to see your changes in the Bitbucket UI by switching branches. 

2. **Pulling to Master**: once you are ready for others to review your code and incorporate it into the master project, you can copy the link from the terminal message after pushing or simply navigate to "Pull requests" on the Bitbucket sidebar. 
In either case, you will use the Bitbucket UI to add team members to review your code and submit the request. 

3. **Reviewing**: if you are added as a reviewer to a pull request, you should navigate to the request in the Bitbucket UI. From here, you can view the diff, add comments, and even edit files directly. 
If you prefer, you can also edit files by checking out the branch in question:  
`$ git checkout -b <branch-name>`  (drop the `-b` flag to checkout an existing branch)  
`$ git pull origin <branch-name>`  
`.... make your changes / add / commit ....`  
`$ git push origin <branch-name>`  
Any changes you have made from your checkout will be added to the existing pull request, and reviewers will be notified.  
You should discuss your desired reviewing workflow as a team. You may choose to simply make comments and allow the original author to implement the changes, or you may decide to edit your teammates' code in place.  
Find a flow that works for you.  

4. **Merging to Master**: once you have the necessary teammate approvals, navigate to the pull request in the Bitbucket UI and click "Merge". 
Verify that any changes made in your branch have now been updated in the master project branch.  

5. **Pulling to Branch**: `$ git pull origin master`  
Everyone in the team should pull the updates down to their branch so that their local code is working from the centralized code base. 


#### Conflicts  

If you and a team member both make changes to the same file, you may end up with [merge conflicts](https://www.atlassian.com/git/tutorials/using-branches/merge-conflicts). 
To deal with this, you will need to open the problematic file in a texteditor, and select the version that you want to keep. 