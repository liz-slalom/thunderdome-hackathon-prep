## R

Whenever possible, use the Project facility of RStudio each time you start working on a new project. 
R Projects make it easy to divide work into multiple contexts, each with their own working directory, workspace, history, and source documents. 
R Projects also make it much easier to share work between collaborators as everything is self-contained.

#### First Steps:
1. From within RStudio, open the `thunderdome.Rproj` file. Always open this file rather than individual scripts in order to load the workspace and all included files.

2. Edit the project-specific .Rprofile `file.edit(".Rprofile")` and paste in the text from the 'Rprofile_template.txt' file. The .Rprofile is where you can manage your environment variables.  For this project, you will need to define these environment variables using the PostgreSQL connection details.

3. Open the launch script (`module.R`).  
Run the script to confirm that your SQL connection works, and use this as a starting place for your analysis. 
Use RStudio to explore perform ad hoc analyses; create your own scripts and directories as needed. 


#### As You Go:
1. You may want to edit `.Rprofile` to include additional project-specific environment variables. If you do so, make sure that you update `Rprofile_template.txt` accordingly, so that other users know which variables need to be created/assigned.  

2. You may need to install additional libraries. Be sure to add these to `module.R`.  

3. Create additional scripts and directories to perform any repeatable procedures and store outputs. Use `source` to load your user-defined functions into `module.R`.  


#### Project files:  
| | |
| --- | --- |
| `Rprofile_template.txt` copy the contents of this file to the project .Rprofile and define accordingly |
| `../.gitignore` | Must include `.Rprofile`, `.RData`, `.Rhistory` |
| `scripts/sql_connect.R` | Connect to the Postgres database and load tables as dataframes |
| `module.R` | A base-level script to test your Postgres connection and begin your analysis |