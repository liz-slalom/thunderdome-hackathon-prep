import os
import psycopg2
import pandas as pd
from sqlalchemy import create_engine
from dotenv import load_dotenv

# Load all environment variables
load_dotenv()

def sql_connection():
    """
    Connect to the SQL database via psycopg2 and sqlalchemy
    """
    engine = create_engine('postgresql://{}:{}@{}/{}'.format(
        os.getenv('db_user'),
        os.getenv('db_password'),
        os.getenv('db_host'),
        os.getenv('db_database')
    ))
    connection = engine.connect()
    return connection

def load_df(table_name, schema, columns=None):
    """
    Create a pandas dataframe from the given table.
    """
    connection = sql_connection()
    df = pd.read_sql_table(
        table_name = table_name,
        con = connection,
        schema = schema,
        columns = columns)
    return df
